# API Server

## Installation and usage

-  With [nodemon]
```
npm install -g nodemon
npm install
npm start
```

-  Without [nodemon]
```
npm install
node server.js
```

After that, server is listening on port 3000 if environment variable `PORT` is not 
set.


## Database connection

Add following lines to *server/config/db.js* file:
```
exports.hosts = '...';
exports.keyspace = '...';
exports.user = '...';
exports.password = '...';
```

## Environment variables

- `PORT` (default: 3000)
- `NODE_ENV` (default: development)

## Authentication

Api uses basic access authentication. To use it, create
*server/config/auth.js* file, then complete following lines with your
credentials and add them do created file.
```
exports.username = '...';
exports.password = '...';
```

## API Routes

Route | HTTP method | Description
--- | --- | --- |
/api/coordinates | GET | Get all coordinates
/api/coordinates/:user_id | GET | Get specific user coordinates
/api/coordinates/:user_id | POST | Add specific user coordinates (details below)
/api/coordinates/last10/:user_id | GET | Get specific user last 10 coordinates
/api/coordinates/last100/:user_id | GET | Get specific user last 100 coordinates
/api/coordinates/last/:limit/:user_id | GET | Get specific user last :limit coordinates
/api/coordinates/:user_id/after/:timestamp | GET | Get all user's coordinates after timestamp (e.g. 1234-01-23T12:34:56+00)
/api/coordinates/:user_id/after/:timestamp/before/:timestamp | GET | Get all user's coordinates from a given period of time

## Possible responses

-  Response with data (status code: 200)
```
{
    "coordinates": [
        {
            "user_id": "...",
            "insertion_time": "...",
            "latitude": "...",
            "longitude": "..."
        }[, ...n] 
    ]
}
```

-  Empty response (status code: 200)
```
{}
```

-  Response to bad request (status code: 400)
```
{
    "error": "details"
}
```

-  Response to request without authorization (status code: 401)
```
Unauthorized
```

-  Response in case of database server error (status code: 500)
```
{
    "error": "details"
}
```

## POST data
```
{
    "data":[
        {
            "time": "1234-01-23 12:34:56+00", 
            "lat": "12.345678",
            "lon": "12.345678"
        }[, ...n]
    ]
}
```

## Logs

Logs are saved in the *server/access.log* file.

[nodemon]:https://github.com/remy/nodemon
