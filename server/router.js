try {
    var config = {};
    config.db = require('./config/db');
} catch (e) {
    console.log('\nError: Database connection configuration file not found\n');
    process.exit(1);
}

var hosts = config.db.hosts,
    keyspace = config.db.keyspace,
    user = config.db.user,
    password = config.db.password;

var router = require('express').Router(),
    cassandra = require('cassandra-driver'),
    authProvider = new cassandra.auth.PlainTextAuthProvider(user, password),
    client = new cassandra.Client({
        contactPoints: [hosts],
        keyspace: keyspace,
        authProvider: authProvider
    });

client.connect(function (err) {
    if (err) {
        console.log('\n' + err + '\n');
        process.exit(1);
    }
});
  
router.get('/', (req, res) => {
    client.execute('select * from coordinates', function (err, result) {
        if (!err) {
            if (result.rows.length > 0) {
                res.json({ coordinates: result.rows });
            } else {
                res.json({});
            }
        } else {
            res.status(500).json({ error: 'database server error' });
        }
    });
});

router.route('/:id').get((req, res) => {
    client.execute('select * from coordinates where user_id = ' + req.params.id,
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid user id' });
            }
        });
}).post((req, res) => {
    var query = 'insert into coordinates (user_id, insertion_time, ' + 
                   'latitude, longitude) values (?, ?, ?, ?);'
    var queries = []
    req.body.data.forEach((data) => {
        queries.push({
            query: query,
            params: [req.params.id, data.time, data.lat, data.lon]
        });
    });
    client.batch(queries, { prepare: true }, (err) => {
        if (!err) { 
            res.json({}); 
        } else { 
            res.status(400).json({ error: 'invalid query' }); 
        }
    });
});

router.get('/last10/:id', (req, res) => {
    client.execute('select * from coordinates where user_id = ' +
        req.params.id + 'limit 10',
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid user id' });
            }
        });
});

router.get('/last100/:id', (req, res) => {
    client.execute('select * from coordinates where user_id = ' +
        req.params.id + 'limit 100',
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid user id' });
            }
        });
});

router.get('/last/:limit/:id', (req, res) => {
    client.execute('select * from coordinates where user_id = ' +
        req.params.id + 'limit ' + req.params.limit,
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid limit or user id' });
            }
        });
});

router.get('/:id/after/:after/before/:before', (req, res) => {
    client.execute('select * from coordinates where user_id = ' +
        req.params.id + " and insertion_time > '" + req.params.after + 
                   "' and insertion_time < '" + req.params.before + "'",
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid timestamp or user id' });
            }
        });
});

router.get('/:id/after/:timestamp', (req, res) => {
    client.execute('select * from coordinates where user_id = ' +
        req.params.id + " and insertion_time > '" + req.params.timestamp + "'",
        function (err, result) {
            if (!err) {
                if (result.rows.length > 0) {
                    res.json({ coordinates: result.rows });
                } else {
                    res.json({});
                }
            } else {
                res.status(400).json({ error: 'invalid timestamp or user id' });
            }
        });
});

module.exports = router;