try {
    var config = {};
    config.auth = require('../config/auth.js');
} catch (e) {
    console.log('\nError: Basic authentication configuration file not found\n');
    process.exit(1);
}

var auth = require('basic-auth');

var username = config.auth.username,
    password = config.auth.password;
    
exports.auth = (req, res, next) => {
    var user = auth(req);
    
    if (!user || user.name !== username || user.pass !== password) {
        res.setHeader('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    }
    next();
};